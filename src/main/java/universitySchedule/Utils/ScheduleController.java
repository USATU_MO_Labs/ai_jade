package universitySchedule.Utils;

import java.util.ArrayList;
import java.util.ArrayList;

/**
 * �����-�������� ��� ��������� ��������� �����.
 * <p>
 * ��� ������ ������������� ���������� ���������������� �����
 * � ������� ������ {@link #init(ArrayList) init}.
 * </p>
 */
public class ScheduleController {
    private static ScheduleController controller;

    public static ScheduleController instance(){
        if(controller == null){
            controller = new ScheduleController();
        }

        return controller;
    }

    private int maxWeek;
    private int maxDay;
    private int maxSeminar;

    private boolean initialized = false;

    private ScheduleController(){
        maxSeminar = 0;
        maxDay = 0;
        maxWeek = 0;
    }

    public void init(ArrayList<Integer> Cfg){
        if(!initialized)
            initialized = true;
        else
            throw new RuntimeException();

        maxSeminar = Cfg.get(0);
        maxDay = Cfg.get(1);
        maxWeek = Cfg.get(2);
    }

    /**
     * �������� ��������� �����
     * @return ������ ��������� �����
     */
    public ArrayList<DateTime> getCleanTimeline() {
        ArrayList<DateTime> res = new ArrayList<>();
        for (int w = 0; w <= maxWeek; w++) {
            for (int d = 1; d <= maxDay; d++) {
                for (int par = 1; par <= maxSeminar; par++) {
                    res.add(new DateTime(w, d, par));
                }
            }
        }
        return res;
    }

    public static ArrayList<DateTime> intersection(ArrayList<DateTime> list1,
                                            ArrayList<DateTime> list2) {// ����������� �������
        ArrayList<DateTime> list = new ArrayList<DateTime>();

        for (DateTime t : list1) {

            if (list2.contains(t)) {
                list.add(t);
            }
        }
        return list;
    }

    public int getMaxSeminar(){
        return maxSeminar;
    }
    public int getMaxDay(){
        return maxDay;
    }
    public int getMaxWeek(){
        return maxWeek;
    }
}
