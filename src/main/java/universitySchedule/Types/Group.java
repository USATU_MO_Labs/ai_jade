package universitySchedule.Types;

import javafx.util.Pair;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Group implements Serializable {
    private String Name;

    public Group(String Name){
        this.Name = Name;
    }

    @Override
    public String toString() {
        return Name;
    }

    public String getName() {
        return Name;
    }
}
