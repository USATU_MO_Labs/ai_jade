package universitySchedule.universityAgents;


import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.Property;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;
import jade.util.Logger;
import javafx.util.Pair;
import universitySchedule.Enums.SubjectType;
import universitySchedule.Types.Seminar;
import universitySchedule.Types.Teacher;
import universitySchedule.Utils.IdGenerator;
import universitySchedule.Utils.DateTime;
import universitySchedule.Utils.ScheduleController;
import universitySchedule.Types.Group;
import universitySchedule.Types.Subject;
import universitySchedule.Utils.MessageContent;

import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.util.*;
import java.util.logging.Level;
import java.util.stream.Collectors;

public class GroupAgent extends Agent {
    private Logger myLogger = Logger.getMyLogger(this.getClass().getName());

    private Group group;
    private ArrayList<Subject> subjects;

    protected void setup() {
        Object[] args = getArguments();
        if (args == null || args.length != 2) {
            myLogger.log(Level.SEVERE, "Couldn't create " + this.getClass().getName());
            doDelete();
            return;
        }

        group = (Group) args[0];
        subjects = (ArrayList<Subject>) args[1];

        // ����������� � ������ ���������
        DFAgentDescription dfd = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();
        sd.setType("GroupAgent");
        sd.setName(this.getName());
        dfd.setName(
                new AID(this.getClass().getName() + group.getName(), false)
        );
        dfd.addServices(sd);

        try {
            DFService.register(this, dfd);
            this.myLogger.log(Level.INFO, this.getLocalName() + " was registered.");
        }
        catch (FIPAException e) {
            this.myLogger.log(Level.SEVERE, this.getLocalName() + " - Cannot register with DF", e);
            doDelete();
        }

        // �������� ��������� ������
        this.addBehaviour(new GroupBehaviour());
    }

    private class GroupBehaviour extends CyclicBehaviour {
        private Logger myLogger = Logger.getMyLogger(this.getClass().getName());

        /**
         * {@link ResultStorageAgent ResultStorageAgent} AID
         */
        private AID resultAgentAID;

        /**
         * {@link TeacherAgent TeacherAgent} AIDs
         */
        private ArrayList<Pair<Teacher, AID>> teacherAIDs;

        /**
         * ������� ������� ���� ������������ �� ������� �������.
         * <ul>
         * <li>Key - ID ��������</li>
         * <li>Value - ��������� ���������� ��� � ����������</li>
         * </ul>
         */
        private Map<Integer, Integer> usedTime = new Hashtable<>();

        /**
         * ��������� ����� ������
         */
        private ArrayList<DateTime> availableSchedule = ScheduleController.instance().getCleanTimeline();

        /**
         * ����������� ����������
         */
        private ArrayList<Seminar> filledSchedule = new ArrayList<>();

        /**
         * ���������� ���������� ������� �� �������������� �� ������� ������� / ConversationId
         * <p>
         * ��� �������� �������������� ������� �� �������� ���� ������ �������� �
         * ����� ������� ����� ����� �� ��� ����. ���� ��� ������������� ������� �
         * �������� ����, ��� ����� ���������� �����������. ��� ����� ����� �����������
         * ������.
         * <p>
         * � ���� ������� ������������� ������ ���������� �������. ������ �������, �������
         * ����������� ��� ������ ������. ���� �� ������ ����, �� ��� ������������� ��������.
         * ��� �� ����� ����� � ���������� ��������������.
         * <p>
         * ����� ������ ������������, ��� ������������� � ����� ������� �� �������� �����
         * ������.
         * </p>
         */
        private Map<String, Integer> conversationRefuseCounts = new Hashtable<>();

        public void onStart() {
            // �������������� �������������� ����� �� ������� ��������
            for (Subject subject : subjects) {
                usedTime.put(subject.getID(), 0);
            }

            // ����� ������ ��� ����� �����������
            resultAgentAID = findResultAgent();

            // ����� ������� ��������
            teacherAIDs = findTeacherAgents();
        }

        private AID findResultAgent() {
            AID resultAgentAID = null;

            DFAgentDescription template = new DFAgentDescription();
            ServiceDescription sd = new ServiceDescription();
            sd.setType("ResultStorageAgent");
            template.addServices(sd);

            SearchConstraints sc = new SearchConstraints();
            sc.setMaxResults(1L);
            try {
                DFAgentDescription[] result = DFService.searchUntilFound(
                        myAgent,
                        myAgent.getDefaultDF(),
                        template,
                        sc,
                        0
                );

                resultAgentAID = result[0].getName();
            }
            catch (FIPAException e) {
                e.printStackTrace();
                myLogger.log(Level.SEVERE, myAgent.getName() + ": ResultStorageAgent not found");
            }
            myLogger.log(Level.SEVERE, myAgent.getName() + ": ResultStorageAgent " + resultAgentAID + " found");
            return resultAgentAID;
        }

        private ArrayList<Pair<Teacher, AID>> findTeacherAgents() {
            ArrayList<Pair<Teacher, AID>> teacherAgents = new ArrayList<>();

            DFAgentDescription template = new DFAgentDescription();
            ServiceDescription sd = new ServiceDescription();
            sd.setType("TeacherAgent");
            template.addServices(sd);
            try {
                DFAgentDescription[] result = DFService.searchUntilFound(
                        myAgent,
                        myAgent.getDefaultDF(),
                        template,
                        null,
                        0);

                for (DFAgentDescription dfd : result) {
                    Iterator sd_it = dfd.getAllServices();
                    while (sd_it.hasNext()) {
                        ServiceDescription result_sd = (ServiceDescription) sd_it.next();
                        Iterator prop_it = result_sd.getAllProperties();
                        while (prop_it.hasNext()) {
                            Property prop = (Property) prop_it.next();
                            if (prop.getName().equals("teacherObject")) {
                                //Teacher teacher = (Teacher) prop.getValue();
                                // JADE ��������, ������-�� � prop.getValue ������ Teacher ���� String
                                Teacher teacher = Teacher.fromMakeshiftSerialized((String) prop.getValue());
                                Pair<Teacher, AID> pair = new Pair<>(teacher, dfd.getName());
                                teacherAgents.add(pair);
                            }
                        }
                    }
                }
            }
            catch (FIPAException e) {
                e.printStackTrace();
                myLogger.log(Level.SEVERE, myAgent.getName() + ": teacherAgents not found");
            }
            catch (ParseException e) {
                e.printStackTrace();
                myLogger.log(Level.SEVERE, "Could not parse teacher from dfd properties");
            }

            return teacherAgents;
        }

        public void action() {
            // �������� ������� ����������
            checkToDie();

            // ���������� �������� ���������
            while (myAgent.getCurQueueSize() > 0) {
                ACLMessage incomingMessage = myAgent.receive();
                if (incomingMessage != null) {
                    processIncomingMessage(incomingMessage);
                }
            }

            // �������� ��������� �������������� ��� ������� ��������
            notifyTeachers();
            block();
        }

        private void checkToDie() {
            long totalSubjects = subjects.size();
            long fullSubjects = subjects.stream()
                    .filter(s -> usedTime.get(s.getID()) >= s.getCount())
                    .count();

            if (fullSubjects == totalSubjects) {
                // ���������� ��������� ���������, ����� ��������� ���������� � �������
                ACLMessage message = new ACLMessage(ACLMessage.PROPOSE);
                try {
                    myLogger.log(Level.INFO, filledSchedule.size() + " seminars");
                    message.setContentObject(filledSchedule);
                    message.addReceiver(resultAgentAID);
                    myAgent.send(message);
                    myLogger.log(Level.INFO, myAgent.getLocalName() + ": message to resultAgent(" + resultAgentAID + ") was send");
                }
                catch (IOException e) {
                    myLogger.log(Level.SEVERE, myAgent.getLocalName() + ": Unable to send data to ResultStorageAgent");
                    e.printStackTrace();
                }
                myLogger.log(Level.INFO, myAgent.getLocalName() + " is dead");
                doDelete();
            }
        }

        private void processIncomingMessage(ACLMessage message) {
            // ������������� ������ � ����� ������ �������� �����
            Seminar seminar;
            try {
                seminar = (Seminar) message.getContentObject();
            }
            catch (UnreadableException e) {
                e.printStackTrace();
                return;
            }

            if (message.getPerformative() == ACLMessage.ACCEPT_PROPOSAL) {
                // ������������� ������� ���� � ����������
                // ������� ������� ����� � �������
                ACLMessage reply = message.createReply();
                reply.setPerformative(ACLMessage.CANCEL);
                try {
                    reply.setContentObject(seminar);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }

                // �������� ����� ��� ������� �����
                // �������� ����� ������ �������������� �� ��� �� �������
                if (filledSchedule.contains(seminar)) {
                    myAgent.send(reply);
                    return;
                }

                // ���� ���� ���-���� �����, ��������� ��
                filledSchedule.add(seminar);
                for (DateTime dt : seminar.datetime) {
                    availableSchedule.remove(dt);
                }
            } else if (message.getPerformative() == ACLMessage.REFUSE) {
                // ������������� ������ �����
                // (�������, ��� ������������� �� �������� REFUSE ������ � ����� ConversationId)
                String conversationId = message.getConversationId();
                int refuseCount = conversationRefuseCounts.get(conversationId);
                // ����� ���� �������
                conversationRefuseCounts.put(conversationId, refuseCount - 1);
                if (refuseCount < 0) {
                    conversationRefuseCounts.remove(conversationId);
                    // ���� ��� ������������� ��������, ������� ���������� ����� �� �������� �����
                    Integer time = usedTime.get(seminar.subject.getID());
                    usedTime.put(seminar.subject.getID(), time - seminar.datetime.length);
                }


            } else {
                // ����
                // ������� �� �������, ����� �� ����:
                // - � ���� �� �����!
                // - ���, � ���� �� �����!
                // - ���, � ����!
                // � �.�.
                myLogger.log(Level.WARNING, "Unknown reply");
                ACLMessage reply = message.createReply();
                reply.setPerformative(ACLMessage.NOT_UNDERSTOOD);
                myAgent.send(reply);
            }
        }

        private void notifyTeachers() {
            // MESSAGE DIARRHEA
            for (Subject subject : subjects) {
                // ���������� ��������, ��� ������� ���� ��������
                if (usedTime.get(subject.getID()) >= subject.getCount()) {
                    continue;
                }

                // ���������� ������� ������� ����
                if (availableSchedule.size() < subject.getLength()) {
                    continue;
                }

                try {
                    // �������� ������ �������
                    String conversationId = IdGenerator.getId("conversation");

                    // ������ �� ���������� ����
                    ACLMessage message = new ACLMessage(ACLMessage.PROPOSE);
                    message.setConversationId(conversationId);
                    MessageContent content = new MessageContent();
                    content.data.put("group", group);
                    content.data.put("subject", subject);
                    content.data.put("availableSchedule", availableSchedule);
                    message.setContentObject(content);

                    // �������� �������������� � �����������
                    // ����� �������������� ����� ����� ��������� CANCEL
                    int teacherCount = 0;
                    for (Pair<Teacher, AID> pair : teacherAIDs) {
                        Integer teacherId = pair.getKey().getID();
                        if (subject.getTeachers().contains(teacherId)) {
                            message.addReceiver(pair.getValue());
                            teacherCount++;
                        }
                    }

                    // ������� ���������� ��������������, ������� ��� ��������� ������
                    conversationRefuseCounts.put(conversationId, teacherCount);

                    myAgent.send(message);

                    // ����� � ���������� �������� ����� ��� ������
                    // ��� �������, allocate space before filling
                    Integer increment = 1;
                    if (subject.getType() == SubjectType.Lab) {
                        increment = 2;
                    }
                    // ����� ��������� �������
                    usedTime.put(subject.getID(), usedTime.get(subject.getID()) + increment);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
