package universitySchedule.parseInput;

import org.json.*;
import universitySchedule.Enums.RoomType;
import universitySchedule.Enums.SubjectType;
import universitySchedule.Types.Group;
import universitySchedule.Types.Room;
import universitySchedule.Types.Subject;
import universitySchedule.Types.Teacher;
import universitySchedule.Utils.ToEnumConverter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;

public class Parser {
    private JSONObject inputObject;

    ArrayList<Room> rooms;
    ArrayList<Teacher> teachers;
    ArrayList<Subject> subjects;
    ArrayList<Group> groups;

    public Parser(String filename){
        File file  = new File(filename);

        FileInputStream fileStream = null;
        try {
            fileStream = new FileInputStream(file);
            JSONTokener tokener = new JSONTokener(fileStream);
            inputObject = new JSONObject(tokener);
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            try {
                fileStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public ArrayList<Room> getRoomsFromJSON(){
        if(rooms != null){
            return rooms;
        }

        JSONArray arr = inputObject.getJSONArray("Rooms");
        rooms = new ArrayList<Room>();

        for(int i = 0; i < arr.length(); i++){
            try {
                JSONObject room = arr.getJSONObject(i);
                RoomType type = ToEnumConverter.StringTypeToRoomType(room.getString("Type"));
                rooms.add(new Room(room.getString("Name"), type));
            }
            catch (ParseException e) {
                e.printStackTrace();
            }

        }

        return rooms;
    }

    public ArrayList<Teacher> getTeachersFromJSON(){
        if(teachers != null){
            return teachers;
        }

        JSONArray arr = inputObject.getJSONArray("Teachers");
        teachers = new ArrayList<Teacher>();

        for(int i = 0; i < arr.length(); i++){
            JSONObject teacher = arr.getJSONObject(i);
            teachers.add(new Teacher(teacher.getInt("ID"), teacher.getString("Name")));
        }

        return teachers;
    }

    public ArrayList<Subject> getSubjectsFromJSON(){
        if(subjects != null){
            return subjects;
        }

        JSONArray arr = inputObject.getJSONArray("Subjects");
        subjects = new ArrayList<Subject>();

        int ID;
        for(ID = 0; ID < arr.length(); ID++){
            JSONObject subj = arr.getJSONObject(ID);
            ArrayList<Integer> teachersID = new ArrayList<>(Arrays.asList(JSONArrayToIntArray(subj.getJSONArray("Teachers"))));
            String rawType = subj.getString("Type");

            try{
                SubjectType type = ToEnumConverter.StringTypeToSubjectTypeEnum(rawType);
                Subject tmpSubject = new Subject(ID, subj.getString("Name"), type, subj.getInt("Count"), teachersID);
                subjects.add(tmpSubject);
            }
            catch (ParseException e){
                e.printStackTrace();
            }
        }

        return subjects;
    }

    public ArrayList<Group> getGroupsFromJSON(){
        if(groups != null){
            return groups;
        }

        JSONArray arr = inputObject.getJSONArray("Groups");
        groups = new ArrayList<Group>();

        for(int i = 0; i < arr.length(); i++){
            String group = arr.getString(i);
            groups.add(new Group(group));
        }

        return groups;
    }

    public ArrayList<Integer> getDateTimeConfigFromJSON(){
        JSONObject cfg = inputObject.getJSONObject("DateTimeConfig");
        ArrayList<Integer> DateTimeCfg = new ArrayList<Integer>();

        DateTimeCfg.add(cfg.getInt("Seminars"));
        DateTimeCfg.add(cfg.getInt("Days"));
        DateTimeCfg.add(cfg.getInt("Weeks"));

        return DateTimeCfg;
    }

    private String[] JSONArrayToStringArray(JSONArray JSONarr){
        String[] arr = new String[JSONarr.length()];

        for(int i = 0; i < JSONarr.length(); i++){
            arr[i] = JSONarr.getString(i);
        }

        return arr;
    }

    private Integer[] JSONArrayToIntArray(JSONArray JSONarr){
        Integer[] arr = new Integer[JSONarr.length()];

        for(int i = 0; i < JSONarr.length(); i++){
            arr[i] = JSONarr.getInt(i);
        }

        return arr;
    }
}
