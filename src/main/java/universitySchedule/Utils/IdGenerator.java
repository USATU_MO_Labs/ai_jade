package universitySchedule.Utils;

import java.util.Hashtable;
import java.util.Map;

public class IdGenerator {
    private static Map<String, Long> ids = new Hashtable<>();

    public static String getId(String key) {
        Long id = ids.getOrDefault(key, 0L);
        ids.put(key, id + 1);

        return id.toString();
    }
}
