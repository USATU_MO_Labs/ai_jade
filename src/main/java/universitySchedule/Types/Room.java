package universitySchedule.Types;

import universitySchedule.Enums.RoomType;

import java.io.Serializable;

public class Room implements Serializable {
    private String Name;
    private RoomType Type;

    public Room(String Name, RoomType Type){
        this.Name = Name;
        this.Type = Type;
    }

    @Override
    public String toString() {
        return Type + "@" + Name;
    }

    public String getName() {
        return Name;
    }

    public RoomType getType() {
        return Type;
    }
}
