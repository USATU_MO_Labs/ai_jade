package universitySchedule.universityAgents;


import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.Property;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;
import jade.util.Logger;
import universitySchedule.Types.*;
import universitySchedule.Utils.DateTime;
import universitySchedule.Utils.MessageContent;
import universitySchedule.Utils.ScheduleController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.stream.Collectors;

public class RoomAgent extends Agent {
	private Logger myLogger = Logger.getMyLogger(this.getClass().getName());

	private Room room;

	protected void setup() {
		Object[] args = getArguments();
		if (args == null || args.length != 1) {
			System.out.println("Error! couldn't create " + this.getClass().getName());
			doDelete();
			return;
		}

		room = (Room) args[0];

		DFAgentDescription dfd = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();
		sd.setType("RoomAgent");
		// Jade ������ �������� � �������� toString, � ��� �� �����������������.
		// �������� ����, ����� �� ���� �������� �� ������ ����.
		sd.addProperties(new Property("roomObject", room.toString()));
		sd.setName(this.getName());
		dfd.setName(
				new AID(this.getClass().getName() + room.getName(), false)
		);
		dfd.addServices(sd);

		try {
			DFService.register(this, dfd);
			this.myLogger.log(Level.INFO, this.getLocalName() + " was registered.");
		}
		catch (FIPAException e) {
			this.myLogger.log(Level.SEVERE, this.getLocalName() + " - Cannot register with DF", e);
			doDelete();
		}

		this.addBehaviour(new RoomBehaviour());
	}

	private class RoomBehaviour extends CyclicBehaviour {
		private Logger myLogger = Logger.getMyLogger(this.getClass().getName());

		/**
		 * ��������� ���������� �������������
		 */
		ArrayList<DateTime> availableSchedule = ScheduleController.instance().getCleanTimeline();

		/**
		 * ����������� ����������
		 */
		private ArrayList<Seminar> filledSchedule = new ArrayList<>();

		@Override
		public void action() {
			// ���������� �������� ���������
			while (myAgent.getCurQueueSize() > 0) {
				ACLMessage incomingMessage = myAgent.receive();
				if (incomingMessage != null) {
					processIncomingMessage(incomingMessage);
				}
			}
			block();
		}

		void processIncomingMessage(ACLMessage message) {
			// ����� ����������������, �� ������ ���� ������ ������ ���������
			// ���������, ��� ������������ (����������) ��� ������ ���������� � ����� ��� ������
			String senderLocalName = message.getSender().getLocalName();
			if (senderLocalName.startsWith(TeacherAgent.class.getName())) {
				// ������ � ���������������
				teacherAgentDialog(message);
			} else if(senderLocalName.startsWith(ResultStorageAgent.class.getName())) {
				// ������ � ���������� �����������
				// ��������� ������ ������ ������ �������
				resultStorageDialog(message);
			} else {
				// ����������� �����������, �������
			}
		}

		private void teacherAgentDialog(ACLMessage message) {
			if (message.getPerformative() == ACLMessage.PROPOSE) {
				teacherAgentProcessPropose(message);
			} else if (message.getPerformative() == ACLMessage.CANCEL) {
				teacherAgentProcessCancel(message);
			} else {
				// ����
			}
		}

		private void teacherAgentProcessPropose(ACLMessage message) {
			// ���� 1: ������������� ���������� ������ �� �������� �������
			// ���������, ����� �� ������� ����. ���� ������� � ����������, ���� ����������.
			try {
				// �������� ���������� �� ���������
				MessageContent content = (MessageContent) message.getContentObject();
				Group group = (Group) content.data.get("group");
				Subject subject = (Subject) content.data.get("subject");
				Teacher teacher = (Teacher) content.data.get("teacher");
				ArrayList<DateTime> teacherSchedule = (ArrayList<DateTime>) content.data.get("availableSchedule");

				// ���������� �������
				Seminar seminar = new Seminar(
						group,
						subject,
						teacher,
						room,
						new DateTime[0]
				);

				// �������� ������� ������ �������
				ArrayList<DateTime> scheduleIntersection = ScheduleController.intersection(availableSchedule, teacherSchedule);
				if (scheduleIntersection.size() >= subject.getLength()) {
					// �������� �����
					ArrayList<DateTime> dts = availableSchedule.stream()
							.limit(subject.getLength())
							.collect(Collectors.toCollection(ArrayList::new));

					availableSchedule.removeAll(dts);

					seminar.datetime = Arrays.copyOf(dts.toArray(), dts.size(), DateTime[].class);

//					seminar.datetime = new DateTime[dts.size()];

//					for(int i = 0; i < dts.size(); i++){
//						Object obj = dts.get(i);
//						seminar.datetime[i] = (DateTime) obj;
//					}

					filledSchedule.add(seminar);

					// ���������� �������
					ACLMessage reply = message.createReply();
					reply.setPerformative(ACLMessage.ACCEPT_PROPOSAL);
					reply.setContentObject(seminar);
					myAgent.send(reply);
				} else {
					// �������� �������
					ACLMessage reply = message.createReply();
					reply.setPerformative(ACLMessage.REFUSE);
					reply.setContentObject(seminar);
					myAgent.send(reply);
				}
			}
			catch (UnreadableException | IOException e) {
				e.printStackTrace();
			}
		}

		private void teacherAgentProcessCancel(ACLMessage message) {
			// ���� 2: ������������� �������� �������� �������
			// ������� ������� � ������� �� ������ ����������. ��������� ������� ���������.
			String conversationId = message.getConversationId();
			Seminar seminar = null;
			try {
				seminar = (Seminar) message.getContentObject();
			}
			catch (UnreadableException e) {
				e.printStackTrace();
			}

			if (!filledSchedule.contains(seminar)) {
				myLogger.log(Level.SEVERE, "Tried to delete seminar from schedule, but it already wasn't there!");
				return;
			}

			// ������ ���� �� ����������
			filledSchedule.remove(seminar);

			// ������� ���� � ������ ���������
			availableSchedule.addAll(Arrays.asList(seminar.datetime));
		}

		private void resultStorageDialog(ACLMessage message) {
			// ���� 3: ��������� �������, ��� ������ ���������, � ����� �������
			myLogger.log(Level.INFO, myAgent.getLocalName() + " is dead");
			doDelete();
		}
	}
}
