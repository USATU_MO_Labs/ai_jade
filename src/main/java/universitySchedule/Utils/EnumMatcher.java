package universitySchedule.Utils;

import universitySchedule.Enums.RoomType;
import universitySchedule.Enums.SubjectType;

public class EnumMatcher {
    public static boolean match(RoomType roomType, SubjectType subjectType) {
        return (roomType == RoomType.Common &&
                (subjectType == SubjectType.Lecture || subjectType == SubjectType.Practice)
        ) || (roomType == RoomType.Laboratory && subjectType == SubjectType.Lab);
    }
}
