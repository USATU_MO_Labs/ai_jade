package universitySchedule.universityAgents;

import jade.core.Agent;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.util.Logger;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;

import java.util.ArrayList;
import java.util.logging.Level;

import universitySchedule.Types.Group;
import universitySchedule.Types.Room;
import universitySchedule.Types.Subject;
import universitySchedule.Types.Teacher;
import universitySchedule.universityAgents.agentArgs.MainAgentArgs;
import universitySchedule.universityAgents.agentArgs.ResultStorageAgentArgs;

public class MainAgent extends Agent {
    private Logger myLogger = Logger.getMyLogger(this.getClass().getName());

    /**
     * ������� ������
     */
    private MainAgentArgs args;

    /**
     * �������� ������ ���������� ��� �������
     *
     * @return ���������� ������ ����������
     */
    private ContainerController getContainer() {
        // Get a hold on JADE runtime
        Runtime rt = Runtime.instance();
        // Create a default profile
        Profile p = new ProfileImpl();
        // Create a new non-main container, connecting to the default
        // main container (i.e. on this host, port 1099)
        ContainerController cc = rt.createAgentContainer(p);
        return cc;
    }

    /**
     * �������� ������� ��������������
     */
    private void createTeachers() {
        ContainerController cc = getContainer();

        ArrayList<Teacher> teachers = args.inputData.getTeachersFromJSON();

        for (Teacher teacher : teachers) {
            String agentName = TeacherAgent.class.getName() + teacher.getID();

            try {
                AgentController controller = cc.createNewAgent(
                        agentName,
                        TeacherAgent.class.getName(),
                        new Object[]{ teacher });

                controller.start();
            }
            catch (StaleProxyException e) {
                myLogger.log(Level.SEVERE, "Can't create agent" + agentName);
            }
        }
    }

    /**
     * �������� ������� ���������
     */
    private void createRooms() {
        ContainerController cc = getContainer();

        ArrayList<Room> rooms = args.inputData.getRoomsFromJSON();

        for (Room room : rooms) {
            String agentName = RoomAgent.class.getName() + room.getName();

            try {
                AgentController controller = cc.createNewAgent(
                        agentName,
                        RoomAgent.class.getName(),
                        new Object[]{ room });

                controller.start();
            }
            catch (StaleProxyException e) {
                myLogger.log(Level.SEVERE, "Can't create agent" + agentName);
            }
        }
    }

    /**
     * �������� ������� �����
     */
    private void createGroups() {
        ContainerController cc = getContainer();

        ArrayList<Group> groups = args.inputData.getGroupsFromJSON();
        ArrayList<Subject> subjects = args.inputData.getSubjectsFromJSON();

        for (Group group : groups) {
            String agName = GroupAgent.class.getName() + group.getName();

            try {
                AgentController controller = cc.createNewAgent(
                        agName,
                        GroupAgent.class.getName(),
                        new Object[]{ group, subjects });

                controller.start();
            }
            catch (StaleProxyException e) {
                myLogger.log(Level.SEVERE, "Cant create agent" + agName);
            }
        }
    }

    /**
     * �������� ������ ��� ����� ����������� ���������.
     */
    private void createResultAgent() {
        ContainerController cc = getContainer();

        try {
            ResultStorageAgentArgs agentArgs = new ResultStorageAgentArgs();
            agentArgs.outputDir = args.outputDir;

            AgentController controller = cc.createNewAgent(
                    ResultStorageAgent.class.getName(),
                    ResultStorageAgent.class.getName(),
                    new Object[]{ agentArgs }
            );

            controller.start();
        }
        catch (StaleProxyException e) {
            myLogger.log(Level.SEVERE, "Can't create agent results");
        }
    }

    /**
     * �������� ������� ��� �������.
     * <p>
     * ������ �������� � ���� ��������� �������:
     * <ul>
     * <li>����������</li>
     * <li>�������</li>
     * <li>���������</li>
     * <li>������</li>
     * </ul>
     * </p>
     */
    private void createServer() {
        myLogger.log(Level.INFO, "Creating server");

        // �������������� � ���� �������, ����� ��� ������ ������ ���� ����� �����
        // �������, �� �� �����
        createRooms();
        createTeachers();
        createGroups();
    }

    /**
     * ������� ������� ��� �������.
     * ������ �������� � ���� ������ ��� ����� �����������.
     * ������ ����� ���� ������ ����.
     */
    private void createClient() {
        myLogger.log(Level.INFO, "Creating client");

        createResultAgent();
    }

    protected void setup() {
        myLogger.log(Level.INFO, "MainAgent is ready to create agents");

        Object[] agentArgs = this.getArguments();

        if (agentArgs == null || agentArgs.length != 1) {
            myLogger.log(Level.SEVERE, "Couldn't create " + this.getClass().getName());
            doDelete();
            return;
        }

        this.args = (MainAgentArgs) agentArgs[0];

        switch (args.mode) {
            case "client":
                this.createClient();
                break;

            case "server":
                this.createServer();
                break;

            default:
                throw new IllegalArgumentException("Invalid mode! must be either 'client' or 'server'");
        }
    }
}
