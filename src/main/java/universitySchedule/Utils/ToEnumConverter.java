package universitySchedule.Utils;

import universitySchedule.Enums.RoomType;
import universitySchedule.Enums.SubjectType;

import java.text.ParseException;

public class ToEnumConverter {
    public static SubjectType StringTypeToSubjectTypeEnum(String s) throws ParseException {
        switch (s) {
            case "Lecture":
                return SubjectType.Lecture;
            case "Practice":
                return SubjectType.Practice;
            case "Lab":
                return SubjectType.Lab;
            default:
                throw new ParseException("Unknown subject type", 0);
        }
    }

    public static RoomType StringTypeToRoomType(String s) throws ParseException {
        switch (s) {
            case "Common":
                return RoomType.Common;
            case "Laboratory":
                return RoomType.Laboratory;
            default:
                throw new ParseException("Unknown room type", 0);
        }
    }
}
