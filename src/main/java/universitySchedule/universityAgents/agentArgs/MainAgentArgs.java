package universitySchedule.universityAgents.agentArgs;

import universitySchedule.parseInput.Parser;

public class MainAgentArgs {
    public String mode;
    public String outputDir;
    public Parser inputData;

    public MainAgentArgs() {}
}
