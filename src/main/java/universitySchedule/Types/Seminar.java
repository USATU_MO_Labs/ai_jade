package universitySchedule.Types;


import universitySchedule.Utils.DateTime;

import java.io.Serializable;

/**
 * �������. ���������� ���������� � ��������, �������������, ������� � ����� ����������.
 */
public class Seminar implements Comparable<Seminar>, Serializable {
    public Group group;
    public Subject subject;
    public Teacher teacher;
    public Room room;
    public DateTime[] datetime;

    public Seminar(
            Group group,
            Subject subject,
            Teacher teacher,
            Room room,
            DateTime[] datetime) {
        this.group = group;
        this.subject = subject;
        this.teacher = teacher;
        this.room = room;
        this.datetime = datetime;
    }

    @Override
    public String toString() {
        String time = "";

        for(DateTime dt : datetime){
            time += dt.toString() + " ";
        }

        return group.toString() + ": " + subject.toString() + ", " + teacher.toString() + ", ���������: " + room.getName() + ", " + time + ";";
    }

    /**
     * �������� �� ���������. ������������ ������ ��� �������� �� ���������.
     * �.�. ���� � �������������� ����������� ��������� �������.
     *
     * @param obj
     * @return
     */
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null || obj.getClass() != this.getClass())
            return false;

        Seminar other = (Seminar) obj;

        boolean datetimeIntersects = false;
        outer:
        for (DateTime this_dt : datetime) {
            for (DateTime other_dt : other.datetime) {
                if (this_dt.equals(other_dt)) {
                    datetimeIntersects = true;
                    break outer;
                }
            }
        }

        return datetimeIntersects;
    }


    @Override
    public int compareTo(Seminar o) {
        // ��������� ������, ����� ���� ������������
        if (this.equals(o)) {
            return 0;
        }

        // � ��������� ������ ��� ���� ������ �������� ����� ������������
        // ������ ��� ������ ���� ��� ������� ��������
        return this.datetime[0].compareTo(o.datetime[0]);
    }
}
