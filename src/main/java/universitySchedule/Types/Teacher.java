package universitySchedule.Types;

import java.io.Serializable;
import java.text.ParseException;

public class Teacher implements Serializable {
    private Integer ID;
    private String Name;

    public Teacher(int ID, String Name){
        this.ID = ID;
        this.Name = Name;
    }

    @Override
    public String toString() {
        return Name;
    }

    public String makeshiftSerialize() {
        return ID.toString() + " " + Name;
    }

    public static Teacher fromMakeshiftSerialized(String teacherStr) throws ParseException {
        String[] arr = teacherStr.split(" ");

        if (arr.length != 2) {
            throw new ParseException("Expected 2 arguments, got " + arr.length, 0);
        }

        int id;
        try {
            id = Integer.parseInt(arr[0]);
        } catch (NumberFormatException e) {
            throw new ParseException("Expected number at 1 argument", 0);
        }

        return new Teacher(id, arr[1]);
    }

    public Integer getID(){
        return ID;
    }
    public String getName(){
        return Name;
    }
}
