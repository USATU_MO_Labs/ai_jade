package universitySchedule.Types;

import universitySchedule.Enums.SubjectType;

import java.io.Serializable;
import java.util.ArrayList;

public class Subject implements Serializable {
    private Integer ID;
    private String Name;
    private SubjectType Type;
    private Integer Count;
    private ArrayList<Integer> Teachers;

    public Subject(int ID, String Name, SubjectType Type, Integer Count, ArrayList<Integer> Teachers) {
        this.ID = ID;
        this.Name = Name;
        this.Type = Type;
        this.Count = Count;
        this.Teachers = Teachers;
    }

    @Override
    public String toString() {
        return Name + " " + Type;
    }

    public int getID() {
        return ID;
    }

    public String getName() {
        return Name;
    }

    public SubjectType getType() {
        return Type;
    }

    public Integer getLength() {
        switch (Type) {
            case Lab:
                return 2;
            case Lecture:
                return 1;
            case Practice:
                return 1;
            default:
                return 0;
        }
    }

    public Integer getCount() {
        return Count;
    }

    public ArrayList<Integer> getTeachers() {
        return Teachers;
    }
}
