package universitySchedule.universityAgents;


import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.Property;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;
import jade.util.Logger;
import javafx.util.Pair;
import universitySchedule.Types.Room;
import universitySchedule.Types.Seminar;
import universitySchedule.Types.Subject;
import universitySchedule.Utils.*;
import universitySchedule.Types.Teacher;

import java.io.IOException;
import java.text.ParseException;
import java.util.*;
import java.util.logging.Level;
import java.util.stream.Collectors;

public class TeacherAgent extends Agent {
    private Logger myLogger = Logger.getMyLogger(this.getClass().getName());

    private Teacher teacher;

    protected void setup() {
        Object[] args = getArguments();
        if (args == null || args.length != 1) {
            System.out.println("Error! couldn't create " + this.getClass().getName());
            doDelete();
            return;
        }

        teacher = (Teacher) args[0];

        DFAgentDescription dfd = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();
        sd.setType("TeacherAgent");
        // Jade ������ �������� toString � ������������ �������
        // ������� �������������� ���������� � ������
        sd.addProperties(new Property("teacherObject", teacher.makeshiftSerialize()));
        sd.setName(this.getName());
        dfd.setName(
                new AID(this.getClass().getName() + teacher.getID(), false)
        );
        dfd.addServices(sd);

        try {
            DFService.register(this, dfd);
            this.myLogger.log(Level.INFO, this.getLocalName() + " was registered.");
        }
        catch (FIPAException e) {
            this.myLogger.log(Level.SEVERE, this.getLocalName() + " - Cannot register with DF", e);
            doDelete();
        }

        this.addBehaviour(new TeacherBehaviour());
    }

    private class TeacherBehaviour extends CyclicBehaviour {
        private Logger myLogger = Logger.getMyLogger(this.getClass().getName());

        /**
         * ������ ���������
         */
        ArrayList<Pair<Room, AID>> roomAIDs;

        /**
         * ��������� ���������� �������������
         */
        ArrayList<DateTime> availableSchedule = ScheduleController.instance().getCleanTimeline();

        /**
         * ����������� ����������
         */
        private ArrayList<Seminar> filledSchedule = new ArrayList<>();

        /**
         * ���������� ���������� ������� �� ��������� �� ������� ������� / ConversationId
         * <p>
         * ��� �������� ���������� ������� �� �������� ���� ������������� �������� �
         * ����� ������� ����� ����� �� ��� ����. ���� ��� ��������� ������� �
         * �������� ����, ��� ����� ���������� �����������. ��� ����� ����� �����������
         * ������.
         * <p>
         * � ���� ������� ������������� ������ ���������� �������. ������ �������, �������
         * ����������� ��� ������ ������. ���� �� ������ ����, �� ��� ������������� ��������.
         * ��� �� ����� ����� � ���������� ��������������.
         * <p>
         * ����� ������ ������������, ��� ������������� � ����� ������� �� �������� �����
         * ������.
         * </p>
         */
        private Map<String, Integer> conversationRefuseCounts = new Hashtable<>();

        /**
         * �������, ������������� ������, �� ������� �������� ���������, � ���������������� ��������.
         * ����� ��� ����, ����� ����� ���� �������� �������, ����� ��������� ������ �� ���������.
         */
        private Map<String, AID> conversationGroupAIDs = new Hashtable<>();

        @Override
        public void onStart() {
            // ����� ������� ���������
            roomAIDs = findRooms();
        }

        private ArrayList<Pair<Room, AID>> findRooms() {
            ArrayList<Pair<Room, AID>> roomAgents = new ArrayList<>();

            // ����� ������� ���������
            DFAgentDescription template = new DFAgentDescription();
            ServiceDescription sd = new ServiceDescription();
            sd.setType("RoomAgent");
            template.addServices(sd);
            try {
                DFAgentDescription[] result = DFService.searchUntilFound(
                        myAgent,
                        myAgent.getDefaultDF(),
                        template,
                        null,
                        0);

                for (DFAgentDescription dfd : result) {
                    Iterator sd_it = dfd.getAllServices();
                    while (sd_it.hasNext()) {
                        ServiceDescription result_sd = (ServiceDescription) sd_it.next();
                        Iterator prop_it = result_sd.getAllProperties();
                        while (prop_it.hasNext()) {
                            Property prop = (Property) prop_it.next();
                            if (prop.getName().equals("roomObject")) {
//                                System.out.println("Cocks" + prop.getValue().toString());
//                                Room room = (Room) prop.getValue();
                                // JADE ��������, ������-�� � prop.getValue ������ Room ���� String
                                String[] arr = ((String) prop.getValue()).split("@");
                                Room room = new Room(arr[1], ToEnumConverter.StringTypeToRoomType(arr[0]));
                                Pair<Room, AID> pair = new Pair<>(room, dfd.getName());
                                roomAgents.add(pair);
                            }
                        }
                    }
                }
            }
            catch (FIPAException e) {
                e.printStackTrace();
                myLogger.log(Level.SEVERE, myAgent.getName() + ": roomAgents not found");
            }
            catch (ParseException e) {
                e.printStackTrace();
            }

            return roomAgents;
        }

        @Override
        public void action() {
            // ���������� �������� ���������
            while (myAgent.getCurQueueSize() > 0) {
                ACLMessage incomingMessage = myAgent.receive();
                if (incomingMessage != null) {
                    processIncomingMessage(incomingMessage);
                }
            }
            block();
        }

        void processIncomingMessage(ACLMessage message) {
            // ����� ����������������, �� ������ ���� ������ ������ ���������
            // ���������, ��� ������������ (����������) ��� ������ ���������� � ����� ��� ������
            String senderLocalName = message.getSender().getLocalName();
            if (senderLocalName.startsWith(GroupAgent.class.getName())) {
                // ������ � ��������
                groupAgentDialog(message);
            } else if (senderLocalName.startsWith(RoomAgent.class.getName())) {
                // ������ � �����������
                roomAgentDialog(message);
            } else if (senderLocalName.startsWith(ResultStorageAgent.class.getName())) {
                // ������ � ���������� �����������
                // ��������� ������ ������ ������ �������
                resultStorageDialog(message);
            } else {
                // ����������� �����������, �������
            }
        }

        private void groupAgentDialog(ACLMessage message) {
            if (message.getPerformative() == ACLMessage.PROPOSE) {
                groupAgentProcessPropose(message);
            } else if (message.getPerformative() == ACLMessage.CANCEL) {
                groupAgentProcessCancel(message);
            } else {
                // ����
            }
        }

        private void groupAgentProcessPropose(ACLMessage message) {
            // ���� 1: ������ ����������� ���������� ���� �� ������� �������� � ������ ��������� ����������.
            // ����� ������� �� ����� ����������� � ���� ��������, ���� ��������� ������ � ���������
            // � ���������������� �������������.
            try {
                // �������� ������ � ������������� �������
                // TODO �������� �������, ���� ����� ������ ��� ����������?
                conversationGroupAIDs.put(message.getConversationId(), message.getSender());

                // ���������� ������ � ���������
                // �������� ���������� �� ���������
                MessageContent content = (MessageContent) message.getContentObject();
                Subject subject = (Subject) content.data.get("subject");
                ArrayList<DateTime> groupSchedule = (ArrayList<DateTime>) content.data.get("availableSchedule");

                // �������� ������� ������ �������
                ArrayList<DateTime> scheduleIntersection = ScheduleController.intersection(availableSchedule, groupSchedule);
                content.data.put("availableSchedule", scheduleIntersection);
                content.data.put("teacher", teacher);

                // ������� ��������� � ������������ � ����� �������
                ArrayList<AID> rooms = roomAIDs.stream()
                        .filter(p -> EnumMatcher.match(p.getKey().getType(), subject.getType()))
                        .map(Pair::getValue)
                        .collect(Collectors.toCollection(ArrayList::new));

                // �������� ���������
                ACLMessage forward = new ACLMessage(ACLMessage.PROPOSE);
                forward.setConversationId(message.getConversationId());
                forward.setContentObject(content);
                for (AID roomAID : rooms) {
                    forward.addReceiver(roomAID);
                }

                myAgent.send(forward);
            }
            catch (UnreadableException e) {
                e.printStackTrace();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }

        private void groupAgentProcessCancel(ACLMessage message) {
            // ���� 2: ������ �������� ������ �� ���������� ������ ����. ���������� ���������� ��
            // �� ������ ���������� � ��������� ������ ��������������� ���������.
            Seminar seminar;
            try {
                seminar = (Seminar) message.getContentObject();
            }
            catch (UnreadableException e) {
                e.printStackTrace();
                return;
            }
            if (!filledSchedule.contains(seminar)) {
                myLogger.log(Level.SEVERE, "Tried to delete seminar from schedule, but it already wasn't there!");
                return;
            }

            // ������ ���� �� ����������
            filledSchedule.remove(seminar);

            // ������� ���� � ������ ���������
            availableSchedule.addAll(Arrays.asList(seminar.datetime));

            // �������� CANCEL ��������������� ����������
            ArrayList<AID> aids = roomAIDs.stream()
                    .filter(p -> p.getKey().getName().equals(seminar.room.getName()))
                    .map(Pair::getValue)
                    .collect(Collectors.toCollection(ArrayList::new));

            ACLMessage forward = new ACLMessage(ACLMessage.CANCEL);
            forward.setConversationId(message.getConversationId());
            try {
                forward.setContentObject(seminar);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            for (AID aid : aids) {
                forward.addReceiver(aid);
            }

            myAgent.send(forward);
        }

        private void roomAgentDialog(ACLMessage message) {
            if (message.getPerformative() == ACLMessage.ACCEPT_PROPOSAL) {
                roomAgentProcessAcceptProposal(message);
            } else if (message.getPerformative() == ACLMessage.REFUSE) {
                roomAgentProcessRefuse(message);
            }
        }

        private void roomAgentProcessAcceptProposal(ACLMessage message) {
            // ���� 3: ��������� ��������� ���� � �������� ������������� �����. ���������� ������� ����
            // �� ��������� � ��������� ������ ���������.
            Seminar seminar;
            try {
                seminar = (Seminar) message.getContentObject();
                // ������������� ������� ���� � ����������
                // ������� ������� ����� � �������

                // �������� ����� ��� ������� �����
                // �������� ����� ������ ��������� �� ��� �� �������
                if (filledSchedule.contains(seminar)) {
                    ACLMessage reply = message.createReply();
                    reply.setPerformative(ACLMessage.CANCEL);
                    myAgent.send(reply);
                    return;
                }

                // ���� ���� ���-���� �����, ��������� ��
                filledSchedule.add(seminar);
                for (DateTime dt : seminar.datetime) {
                    availableSchedule.remove(dt);
                }

                // ���������� ������ �������
                ACLMessage msgToGroup = new ACLMessage(ACLMessage.ACCEPT_PROPOSAL);

                msgToGroup.setContentObject(seminar);
                msgToGroup.addReceiver(conversationGroupAIDs.get(message.getConversationId()));
                myAgent.send(msgToGroup);
            }
            catch (UnreadableException | IOException e) {
                e.printStackTrace();
                myLogger.log(Level.SEVERE, "Something wrong");
            }
        }

        private void roomAgentProcessRefuse(ACLMessage message) {
            // ���� 4: ��������� �������� � ���������� ����.
            Seminar seminar;
            try {
                seminar = (Seminar) message.getContentObject();
            }
            catch (UnreadableException e) {
                e.printStackTrace();
                return;
            }

            // ��������� ������� �����
            // (�������, ��� ��������� �� �������� REFUSE ������ � ����� ConversationId)
            String conversationId = message.getConversationId();
            int refuseCount = conversationRefuseCounts.get(conversationId);
            // ����� ���� �������
            conversationRefuseCounts.put(conversationId, refuseCount - 1);
            if (refuseCount < 0) {
                conversationRefuseCounts.remove(conversationId);

                // ���� ��� ��������� ��������, �������� ����� ������
                AID groupAID = conversationGroupAIDs.get(conversationId);
                conversationGroupAIDs.remove(conversationId);

                ACLMessage groupRefuseMessage = new ACLMessage(ACLMessage.REFUSE);
                groupRefuseMessage.setConversationId(conversationId);
                try {
                    groupRefuseMessage.setContentObject(seminar);
                }
                catch (IOException e) {
                    myLogger.log(Level.SEVERE, "Couldn't send REFUSE to group");
                    e.printStackTrace();
                }

                groupRefuseMessage.addReceiver(groupAID);

                myAgent.send(groupRefuseMessage);
            }
        }

        private void resultStorageDialog(ACLMessage message) {
            // ���� 5: ������������� �������, ��� ������ ���������, � ����� �������.
            myLogger.log(Level.INFO, myAgent.getLocalName() + " is dead");
            doDelete();
        }
    }
}
