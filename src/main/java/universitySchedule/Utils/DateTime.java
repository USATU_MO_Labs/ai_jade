package universitySchedule.Utils;

import java.io.Serializable;

public class DateTime implements Serializable, Comparable<DateTime> {
    private int Seminar;// 1-6 #���� ��� ���
    private int Day;// 1-6 # ��� � ������
    private int Week;// 1-18 # ������� ������

    public DateTime(int week, int day, int Seminar) {
        this.Day = day;
        this.Seminar = Seminar;
        this.Week = week;
    }

    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null || obj.getClass() != this.getClass())
            return false;

        DateTime dt = (DateTime) obj;

        return (this.Seminar == dt.Seminar) && (this.Day == dt.Day)
                && (this.Week == dt.Week);

    }

    public String toString() {//� ������
        return "������: " + Week + ", ����:" + Day + ", ����: " + Seminar;
    }

    public boolean greaterThen(DateTime dt2) {//��������� ���
        // TODO remove in favour of CompareTo
        if (Week > dt2.Week)
            return true;
        if (Week == dt2.Week && Day > dt2.Day) {
            return true;
        }
        return Week == dt2.Week && Day == dt2.Day && Seminar > dt2.Seminar;
    }

    public DateTime nextSeminar() {//��������� ���� ���� �� ���
        DateTime dt2 = new DateTime(this.Week, this.Day, this.Seminar);
        dt2.Seminar++;
        if (dt2.Seminar > ScheduleController.instance().getMaxSeminar()) {
            return null;
        }
        return dt2;
    }

    public DateTime previousSeminar() {//���������� ���� ���� �� ���
        DateTime dt2 = new DateTime(this.Week, this.Day, this.Seminar);
        dt2.Seminar--;
        if (dt2.Seminar <= 0) {
            return null;
        }
        return dt2;
    }

    @Override
    public int compareTo(DateTime o) {
        if (this.equals(o)) {
            return 0;
        }

        boolean thisGreaterThanOther = (Week > o.Week) ||
                (Week == o.Week && Day > o.Day) ||
                (Week == o.Week && Day == o.Day && Seminar > o.Seminar);

        return thisGreaterThanOther ? +1 : -1;
    }
}
