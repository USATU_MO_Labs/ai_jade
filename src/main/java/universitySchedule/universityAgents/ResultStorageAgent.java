package universitySchedule.universityAgents;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;
import jade.util.Logger;
import universitySchedule.Types.Seminar;
import universitySchedule.universityAgents.agentArgs.ResultStorageAgentArgs;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;
import java.util.logging.Level;

// TODO ���������� ��� �����
// ������������ �������� ������� � ����������� ��������� �����
// ������������ ����������
// �������� ����� � ��������� ����?

public class ResultStorageAgent extends Agent {
    private Logger myLogger = Logger.getMyLogger(this.getClass().getName());

    /**
     * �������� ��������� ��� ������
     */
    private ResultStorageAgentArgs args;

    /**
     * ������ ���������
     */
    private ArrayList<AID> rooms;

    /**
     * ������ ��������������
     */
    private ArrayList<AID> teachers;

    /**
     * ������ �����
     */
    private ArrayList<AID> groups;

    /**
     * ������ ��������� ���� ���
     */
    private ArrayList<Seminar> allSeminars = new ArrayList<>();

    /**
     * ���������� ������� �����
     */
    private int deadGroups;

    protected void setup() {
        Object[] agentArgs = getArguments();
        if (agentArgs == null || agentArgs.length != 1) {
            myLogger.log(Level.SEVERE, "Couldn't create " + this.getClass().getName());
            doDelete();
            return;
        }

        args = (ResultStorageAgentArgs) agentArgs[0];

        DFAgentDescription dfd = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();
        sd.setType("ResultStorageAgent");
        sd.setName(this.getName());
        dfd.setName(
                new AID(this.getClass().getName(), false)
        );
        dfd.addServices(sd);

        try {
            DFService.register(this, dfd);
            this.myLogger.log(Level.INFO, this.getLocalName() + " was registered.");
        }
        catch (FIPAException e) {
            this.myLogger.log(Level.SEVERE, this.getLocalName() + " - Cannot register with DF", e);
            doDelete();
        }

        addBehaviour(new ResultStorageBehaviour());
    }

    private class ResultStorageBehaviour extends CyclicBehaviour {
        @Override
        public void onStart() {
            rooms = findRooms();
            teachers = findTeachers();
            groups = findGroups();
        }

        private ArrayList<AID> findRooms() {
            ArrayList<AID> agents = new ArrayList<>();

            // ����� ������� ���������
            DFAgentDescription template = new DFAgentDescription();
            ServiceDescription sd = new ServiceDescription();
            sd.setType("RoomAgent");
            template.addServices(sd);
            try {
                DFAgentDescription[] result = DFService.searchUntilFound(
                        myAgent,
                        myAgent.getDefaultDF(),
                        template,
                        null,
                        0);

                for (DFAgentDescription dfd : result) {
                    agents.add(dfd.getName());
                }
            }
            catch (FIPAException e) {
                e.printStackTrace();
                myLogger.log(Level.SEVERE, myAgent.getName() + ": roomAgents not found");
            }
            myLogger.log(Level.INFO, myAgent.getLocalName() + " found some rooms: " + agents.size());
            return agents;
        }

        private ArrayList<AID> findTeachers() {
            ArrayList<AID> agents = new ArrayList<>();

            // ����� ������� ���������
            DFAgentDescription template = new DFAgentDescription();
            ServiceDescription sd = new ServiceDescription();
            sd.setType("TeacherAgent");
            template.addServices(sd);
            try {
                DFAgentDescription[] result = DFService.searchUntilFound(
                        myAgent,
                        myAgent.getDefaultDF(),
                        template,
                        null,
                        0);

                for (DFAgentDescription dfd : result) {
                    agents.add(dfd.getName());
                }
            }
            catch (FIPAException e) {
                e.printStackTrace();
                myLogger.log(Level.SEVERE, myAgent.getName() + ": teacherAgents not found");
            }
            myLogger.log(Level.INFO, myAgent.getLocalName() + " found some teachers: " + agents.size());
            return agents;
        }

        private ArrayList<AID> findGroups() {
            ArrayList<AID> agents = new ArrayList<>();

            // ����� ������� ���������
            DFAgentDescription template = new DFAgentDescription();
            ServiceDescription sd = new ServiceDescription();
            sd.setType("GroupAgent");
            template.addServices(sd);
            try {
                DFAgentDescription[] result = DFService.searchUntilFound(
                        myAgent,
                        myAgent.getDefaultDF(),
                        template,
                        null,
                        0);

                for (DFAgentDescription dfd : result) {
                    agents.add(dfd.getName());
                }
            }
            catch (FIPAException e) {
                e.printStackTrace();
                myLogger.log(Level.SEVERE, myAgent.getName() + ": groupAgents not found");
            }
            myLogger.log(Level.INFO, myAgent.getLocalName() + " found some groups: " + agents.size());
            return agents;
        }

        @Override
        public void action() {
			/*
			�������, ����� ��� ������ ������� � ���������� ������ � ������� ������.
			����� ����� ������ ������� �������������� � ��������� ����������������.
			����� ��� ���������� ����� �������, ������� �� � �������� �����.
			� ���������� ������ ���������� ��������� �����:
			- ������
				- ���������� 1
				- ���������� 2
				- ...
			- �������������
				- ���������� 1
				- ���������� 2
				- ...
			- ���������
				- ���������� 1
				- ���������� 2
				- ...
			*/
            // ���������� �������� ���������
            while (myAgent.getCurQueueSize() > 0) {

                ACLMessage incomingMessage = myAgent.receive();
                if (incomingMessage != null) {
                    processIncomingMessage(incomingMessage);
                }
            }
            block();
        }

        private void processIncomingMessage(ACLMessage message) {
            myLogger.log(Level.INFO, myAgent.getLocalName() + " I got a message from " + message.getSender());

            // �������, ��� � ��� ����� ��������� ��������� ������ �� �����
            try {
                ArrayList<Seminar> content = (ArrayList<Seminar>) message.getContentObject();
                allSeminars.addAll(content);
                deadGroups++;
                myLogger.log(Level.INFO, "Another group is dead. Remains: " + (groups.size() - deadGroups));
                if (deadGroups == groups.size()) {
                    myLogger.log(Level.INFO, "All groups are dead. Killing remaining agents.");
                    // ��� ������ ���������, ����� �������� ��������� ������� � �������� ������
                    ACLMessage killswitch = new ACLMessage(ACLMessage.PROPOSE);

                    for (AID aid : rooms) {
                        killswitch.addReceiver(aid);
                    }

                    for (AID aid : teachers) {
                        killswitch.addReceiver(aid);
                    }

                    myAgent.send(killswitch);

                    writeResultData();


                    myLogger.log(Level.INFO, "All agents are dead. Committing suicide.");
                    doDelete();
                }
            }
            catch (UnreadableException e) {
                e.printStackTrace();
            }
        }

        private void writeResultData() {
            allSeminars.sort(Seminar::compareTo);
            myLogger.log(Level.INFO, allSeminars.size() + " seminars");

            Map<String, ArrayList<Seminar>> groups = new Hashtable<>();
            Map<String, ArrayList<Seminar>> teachers = new Hashtable<>();
            Map<String, ArrayList<Seminar>> rooms = new Hashtable<>();

            for (Seminar seminar : allSeminars) {
                String groupKey = seminar.group.getName();
                groups.putIfAbsent(groupKey, new ArrayList<Seminar>());
                groups.get(groupKey).add(seminar);

                String teacherKey = seminar.teacher.getID().toString();
                teachers.putIfAbsent(teacherKey, new ArrayList<Seminar>());
                teachers.get(teacherKey).add(seminar);

                String roomKey = seminar.room.getName();
                rooms.putIfAbsent(roomKey, new ArrayList<Seminar>());
                rooms.get(roomKey).add(seminar);
            }

            writeMappedSeminars("groups", groups);
            writeMappedSeminars("teachers", teachers);
            writeMappedSeminars("rooms", rooms);
        }

        private void writeMappedSeminars(String dir, Map<String, ArrayList<Seminar>> seminarMap) {
            try {
                Path dirPath = Paths.get(args.outputDir, dir);
                if (!Files.exists(dirPath))
                    Files.createDirectory(dirPath);
            }
            catch (IOException e) {
                e.printStackTrace();
            }

            for (String key : seminarMap.keySet()) {
                Path filePath = Paths.get(args.outputDir, dir, key + ".txt");

                try {
                    if (!Files.exists(filePath))
                        Files.createFile(filePath);

                    BufferedWriter writer = Files.newBufferedWriter(filePath, Charset.forName("UTF-8"));
                    for (Seminar seminar : seminarMap.get(key)) {
                        writer.write(seminar.toString() + "\n");
                    }
                    writer.close();
                }
                catch (IOException e) {
                    myLogger.log(Level.SEVERE, "Unable to write results to " + filePath.toString());
                    e.printStackTrace();
                }
            }
        }
    }
}
