package universitySchedule;

import jade.Boot;
import jade.MicroBoot;
import jade.core.MicroRuntime;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import universitySchedule.universityAgents.agentArgs.MainAgentArgs;
import universitySchedule.Utils.ScheduleController;
import universitySchedule.parseInput.Parser;
import universitySchedule.universityAgents.MainAgent;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


public class Main {
    public static void main(String[] args) {
        Boot.main(args);
        JFrame frame = new MainForm();
        frame.setVisible(true);
    }

    static void startAgents(MainAgentArgs args) throws Exception {
        try {
            // �������������� ���������� ���������� (���� �� �����)
            if (args.mode == "server") {
                ScheduleController.instance().init(args.inputData.getDateTimeConfigFromJSON());
            }

            // Get a hold on JADE runtime
            Runtime rt = Runtime.instance();

            ProfileImpl p = new ProfileImpl(false);

            AgentContainer agentContainer = rt.createAgentContainer(p);
            AgentController agent = agentContainer.createNewAgent(
                    "agent_" + args.mode,
                    MainAgent.class.getName(),
                    new Object[]{ args });

            agent.start();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static class MainForm extends JFrame {
        InputDialog inputDialog;

        MainForm() {
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            inputDialog = new InputDialog();
            this.setContentPane(inputDialog.view);

            this.pack();

            inputDialog.okCallback = () -> {
                MainAgentArgs args = new MainAgentArgs();
                args.mode = inputDialog.mode;

                if (args.mode.equals("server")) {
                    args.inputData = new Parser(inputDialog.inputFile);
                }

                args.outputDir = inputDialog.outputDir;

                startAgents(args);

                return null;
            };

            addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    MicroRuntime.stopJADE();
                }
            });
        }
    }
}
